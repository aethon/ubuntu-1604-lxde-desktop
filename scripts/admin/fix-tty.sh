#!/bin/bash

# fix issue with vagrant provisioners https://github.com/mitchellh/vagrant/issues/1673
grep -q 'mesg n' /root/.profile && sed -i '/mesg n/d' /root/.profile
