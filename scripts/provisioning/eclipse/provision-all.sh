#!/bin/bash

# execute all known provisioners in order
export ECLIPSE_FLAVOR=$1
bash eclipse.sh || exit 1
bash groovy-plugin.sh || exit 1
bash ivy-plugin.sh || exit 1
bash eclemma.sh || exit 1
bash import-projects-plugin.sh || exit 1
