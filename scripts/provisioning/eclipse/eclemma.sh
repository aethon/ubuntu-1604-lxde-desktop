#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

SOURCE=http://update.eclemma.org/
ensure_feature "EclEmma" $SOURCE \
  com.mountainminds.eclemma.feature.feature.group || exit 1
