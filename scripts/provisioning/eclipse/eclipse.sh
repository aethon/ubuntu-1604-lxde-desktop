#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

case $ECLIPSE_FLAVOR
  "neon" ) ECLIPSE_SOURCE=http://eclipse.mirror.rafal.ca/technology/epp/downloads/release/neon/R/eclipse-java-neon-R-linux-gtk-x86_64.tar.gz ;;
  "mars2") ECLIPSE_SOURCE=http://eclipse.mirror.rafal.ca/technology/epp/downloads/release/mars/2/eclipse-java-mars-2-linux-gtk-x86_64.tar.gz ;;
esac

if [ -f $ECLIPSE_HOME ]; then
  echo Eclipse ($ECLIPSE_FLAVOR) already installed
else
  echo Installing Eclipse ($ECLIPSE_FLAVOR)

  # download from the official site
  ECLIPSE_FN="$DOWNLOADS/$( basename $ECLIPSE_SOURCE )"
  [ -f "$ECLIPSE_FN" ] || curl -L -o "$ECLIPSE_FN" --create-dirs $ECLIPSE_SOURCE || exit 1

  # extract as a local install
  mkdir -p "$FLAVOR_HOME" || exit 1
  tar -zxvf "$ECLIPSE_FN" -C "$FLAVOR_HOME" || exit 1

  # copy config
  cp -f "$ECLIPSE_FLAVOR/eclipse.ini" "$ECLIPSE_HOME/eclipse.ini" || exit 1

  echo Eclipse ($ECLIPSE_FLAVOR) installed
fi

if [ -f ~/Desktop/eclipse-$ECLIPSE_FLAVOR.desktop ]; then
  echo Eclipse ($ECLIPSE_FLAVOR) icon already installed
else
  echo Installing Eclipse ($ECLIPSE_FLAVOR) icon

  [ -d ~/Desktop ] || mkdir ~/Desktop || exit 1
  tee ~/Desktop/eclipse-$ECLIPSE_FLAVOR.desktop <<-FILE
[Desktop Entry]
Name=Eclipse ($ECLIPSE_FLAVOR)
Type=Application
Exec=$ECLIPSE_HOME/eclipse
Terminal=false
Icon=$ECLIPSE_HOME/icon.xpm
Comment=Integrated Development Environment
NoDisplay=false
Categories=Development;IDE;
Name[en]=Eclipse ($ECLIPSE_FLAVOR)
Name[en_US]=Eclipse ($ECLIPSE_FLAVOR)
FILE
  chmod +x ~/Desktop/eclipse-$ECLIPSE_FLAVOR.desktop || exit 1

  echo Eclipse ($ECLIPSE_FLAVOR) icon installed
fi
