tee /etc/lightdm/lightdm.conf <<-file
  [Seat:*]
  autologin-user=$SSH_USERNAME
  autologin-user-timeout=0
  user-session=Lubuntu
  greeter-session=lightdm-gtk-greeter
file
