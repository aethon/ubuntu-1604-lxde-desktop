#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"

packer build packer.json || exit 1
( cd .. && vagrant box add --force --name code42/udev packer/build/code42-udev.box ) || exit 1

echo "Box build completed; added to vagrant"
