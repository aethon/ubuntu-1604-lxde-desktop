#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

IVY_SOURCE=http://artifactory.corp.code42.com/artifactory/libs-release/org/apache/ivy/ivy/2.4.0/ivy-2.4.0.jar
IVY_DEST="$HOME/.ant/lib/$( basename $IVY_SOURCE )"

if [ -f $IVY_DEST ]; then
  echo Ivy already installed
else
  echo Installing Ivy

  # copy Ivy to Ant
  curl -L -o "$IVY_DEST" --create-dirs $IVY_SOURCE || exit 1

  echo Ivy installed
fi
