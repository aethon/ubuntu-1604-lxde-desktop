#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

SOURCE=http://testng.org/eclipse-beta
ensure_feature "TestNG plugin" $SOURCE \
  org.testng.eclipse.feature.group || exit 1
