#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

# See https://github.com/seeq12/eclipse-import-projects-plugin
SOURCE=https://github.com/seeq12/eclipse-import-projects-plugin/blob/master/jar/com.seeq.eclipse.importprojects_1.3.0.jar?raw=true
PLUGIN="$ECLIPSE_HOME/plugins/com.seeq.eclipse.importprojects_1.3.0.jar"

if [ -f "$PLUGIN" ]; then
  echo Import Projects plugin already installed
else
  echo Installing Import Projects plugin
  curl -L -o "$PLUGIN" --create-dirs $SOURCE
  echo Import Projects plugin installed
fi
