add-apt-repository ppa:webupd8team/java -y # java 8 source
apt-get update -y
#DEBIAN_FRONTEND=noninteractive apt-get upgrade -y -o Dpkg::Options::="--force-confdef"

### java 8
# accept the Oracle license agreement
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
# and install JDK 8
apt-get install oracle-java8-installer -y
apt-get install oracle-java8-set-default -y
###

### git
apt-get install git -y
###

### python
apt-get install python -y
###

### build essentials
apt-get install build-essential -y
###

### Light locker (turn off screen locking)
sudo apt-get remove light-locker -y
###

### Postgres
sudo apt-get install postgresql -y
sudo apt-get install postgresql-contrib -y
sudo apt-get install pgadmin3 -y
###
