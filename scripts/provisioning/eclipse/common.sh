#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. ../common.sh

function needs_feature() {
  grep -q "id=\"$1\"" "$ECLIPSE_HOME/configuration/org.eclipse.update/platform.xml" >/dev/null \
  && return 0 \
  || return 1
}
export has_feature;

export ensure_feature() {
  local name="$1"
  local repo="$2"
  local all=( "$@" )
  local IUs=${all[@]:2}

  local missing=
  for IU in IUs
  do
    if needs_feature $IU; then
      missing="-installIU $IU $missing"
    fi
  done
  if [ -n "$missing" ]; then
    echo Installing $name

    "$ECLIPSE_HOME/eclipse" -nosplash \
      -application org.eclipse.equinox.p2.director \
      -repository $repo \
      -destination "$ECLIPSE_HOME" \
      $missing \
      -profileProperties org.eclipse.update.install.features=true \
      || return 1

      echo Installed $name
  else
      echo $name is already installed
  fi
}
export ensure_feature

export ECLIPSE_FLAVOR=${$1:-"${$ECLIPSE_FLAVOR:-neon}"}
case $ECLIPSE_FLAVOR
  "neon" );;
  "mars2");;
  *)
  echo "Unknown Eclipse flavor indicated: ${ECLIPSE_FLAVOR}"
  exit 1
esac

### folders
export ECLIPSE_DEST=~/.local/share
export FLAVOR_HOME="$ECLIPSE_DEST/eclipse-$ECLIPSE_FLAVOR"
export ECLIPSE_HOME="$FLAVOR_HOME/eclipse"
