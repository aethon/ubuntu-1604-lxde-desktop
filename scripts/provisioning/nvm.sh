#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

export NVM_DIR=~/.nvm
if nvm --version >/dev/null; then
  echo NVM already installed
else
  echo Installing NVM

  # use insecure installer
  # (by default, nvm installer sources nvm in the .bashrc file. this does not
  # support login shells, as is used by web-build-all.sh. so, intercept the
  # sourcing code and apply it to both .bashrc and .profile)
  PROFILE=~/.nvmprofile
  echo "" >"$PROFILE"
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash || exit 1
  command grep -qFc '/nvm.sh' ~/.profile || cat "$PROFILE" >>~/.profile
  command grep -qFc '/nvm.sh' ~/.bashrc || cat "$PROFILE" >>~/.bashrc
  rm "$PROFILE"

  # life will be better if the npm cache is stored on the host
  echo "cache=/host/.npm" >>~/.npmrc

  echo NVM installed
fi
