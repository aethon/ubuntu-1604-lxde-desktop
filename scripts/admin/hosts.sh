#!/bin/bash
exit
cd "$( dirname "${BASH_SOURCE[0]}" )"
echo $pwd
exit
# load entries and convert to regular expression
PATTERN=$( sed '/^\s*$/d' <data/entries \
  | tr "\n\r" "||" \
  | sed 's/[|]$//' \
  | sed 's/[|]/\\|/g' \
  | sed 's/\./\\./g' )

# remove existing entries from hosts
#  and add all entries back into hosts with home address
echo -e "$( sed  "/^\s*[^#].*\(${PATTERN}\)\s*$/d" /etc/hosts )"'\n'"$( sed '/^\s*$/d' <data/entries | sed 's/^\s*\S.*$/127.0.0.1 &/' )" >/etc/hosts || exit 1

# ensure well known hosts
diff --new-line-format="" --unchanged-line-format="" <(sort data/known_hosts) <(sort ~/.ssh/known_hosts) >>~/.ssh/known_hosts || exit 1
