#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

# write the java version to buildinit.rc to support the build.
# BAD BAD BAD the environment variable should not be tied to a version
[ -d ~/bin ] || mkdir ~/bin
grep -qF "JAVA8_91=\"$JAVA_HOME\"" || echo "JAVA8_91=\"$JAVA_HOME\"" >> ~/bin/buildinit.rc || exit 1
