#!/bin/bash

DB_HOME="/usr/local/var"
DB=$DB_HOME/postgres
PG_CONF="$DB/postgresql.conf"

if [ -f $DB_HOME/postgresql.conf ]; then
  echo Postgres is already configured
else
  echo Configuring Postgres

  # add the path in the rc scripts
  PG_HOME=$( dirname $( find /usr -name postgres ) )
  CMD="echo \$PATH | grep -q \"$PG_HOME\" || PATH=$PG_HOME:\$PATH"
  grep -qF "$CMD" ~/.bashrc || echo $CMD >>~/.bashrc || exit 1
  grep -qF "$CMD" ~/.profile || echo $CMD >>~/.profile || exit 1

  # and use the path here, too
  eval $CMD
  export PATH

  [ -d "$DB_HOME" ] || mkdir -p "$DB_HOME" || exit 1
  chown -R postgres "$DB_HOME"

  # init the instance
  su - postgres -m -c "initdb -U postgres -D \"$DB\"" || exit 1

  # create the user
  su - postgres -m -c "echo \"CREATE USER c42 WITH SUPERUSER PASSWORD 'c422006'\" | psql -U postgres" || exit 1

  # fix up this option
  PG_CONF=$DB/postgresql.conf
  if grep -q "^\s*bytea_output\s*=.*$" "$PG_CONF"; then
    sed "s/^\s*bytea_output\s*=.*$/bytea_output = 'escape'/" || exit 1
  else
    echo "bytea_output = 'escape'" >>"$PG_CONF" || exit 1
  fi

  # launch postgres at boot
  sudo update-rc.d postgresql enable

  echo Postgres installed
fi
