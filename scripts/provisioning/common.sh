#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"
export VAGRANTDIR=/vagrant
export CONFIGDIR="$VAGRANTDIR/config"

export DOWNLOADS="$HOME/.downloads"
[ -d "$DOWNLOADS" ] || mkdir "$DOWNLOADS"
