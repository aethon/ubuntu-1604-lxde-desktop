#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

ANT_SOURCE=http://apache.mesi.com.ar/ant/binaries/apache-ant-1.9.7-bin.tar.gz
ANT_DEST=~/.local/share/ant
ANT_DIR="$ANT_DEST/$( echo $(basename $ANT_SOURCE) | sed 's|-bin\.tar\.gz$||' )"

if [ -d $ANT_DIR ]; then
  echo Ant already installed
else
  echo Installing Ant

  # download from the official site
  ANT_FN="$DOWNLOADS/$(basename $ANT_SOURCE)"
  [ -f "$ANT_FN" ] || curl -L -o "$ANT_FN" --create-dirs $ANT_SOURCE || exit 1

  # extract as a local install
  mkdir -p "$ANT_DIR"
  tar -zxvf "$ANT_FN" -C "$ANT_DEST" || exit 1

  # write an rc script
  cat >~/.antrc <<-FILE || exit 1
    export ANT_OPTS="-Xmx1024m -Xms512m"
    export ANT_HOME="$ANT_DIR"
    export PATH="$ANT_DIR/bin:\$PATH"
FILE

  # point the rc scripts at it
  CMD='[ -f ~/.antrc ] && . ~/.antrc'
  grep -qF "$CMD" ~/.bashrc || echo $CMD >>~/.bashrc
  grep -qF "$CMD" ~/.profile || echo $CMD >>~/.profile

  echo Ant installed
fi
