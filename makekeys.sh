#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"

# generate the key if required
[ -f config/id_rsa.pub -a -f config/id_rsa ] || ssh-keygen -t rsa -f config/id_rsa  || exit 1

# copy to the clipboard if supported
if which pbcopy >/dev/null; then
  pbcopy <config/id_rsa.pub
  CLIP="on the clipboard and "
fi

# feedback
echo "Your VM's public ssh key is ${CLIP}at $(pwd)/work/id_rsa.pub"
