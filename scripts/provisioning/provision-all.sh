#!/bin/bash

# execute all known provisioners in order
bash eclipse/provision-all.sh "neon" || exit 1
bash eclipse/provision-all.sh "mars2" || exit 1
bash ant.sh || exit 1
bash ivy.sh || exit 1
bash rvm.sh || exit 1
bash nvm.sh || exit 1
bash buildenv.sh || exit 1
