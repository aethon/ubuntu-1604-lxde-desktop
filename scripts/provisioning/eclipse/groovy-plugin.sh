#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

case $ECLIPSE_FLAVOR in
  "neon"  ) SOURCE=http://dist.springsource.org/snapshot/GRECLIPSE/e4.6/ ;;
  "mars2" ) SOURCE=http://dist.springsource.org/snapshot/GRECLIPSE/e4.5/ ;;
  *       )
    echo "Unknown Eclipse flavor: \"${ECLIPSE_FLAVOR}\""
    exit 1
esac

ensure_feature "Groovy plugin" $SOURCE \
  org.codehaus.groovy24.feature.feature.group \
  org.codehaus.groovy.eclipse.feature.feature.group \
