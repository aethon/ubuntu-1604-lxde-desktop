#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

if rvm --version >/dev/null; then
  echo Ruby/RVM already installed
else
  echo Installing Ruby/RVM

  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 || exit 1
  curl -sSL https://get.rvm.io | bash -s stable --ruby=1.9.3|| exit 1

  # point the rc scripts at it
  CMD='[ -f /etc/profile.d/rvm.sh ] && . /etc/profile.d/rvm.sh'
  grep -qF "$CMD" ~/.bashrc || echo $CMD >>~/.bashrc || exit 1

  echo Ruby/RVM installed
fi
