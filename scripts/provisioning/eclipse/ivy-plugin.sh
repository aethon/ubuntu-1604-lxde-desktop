#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]} ")"
. common.sh

SOURCE=http://www.apache.org/dist/ant/ivyde/updatesite/

ensure_feature "Ivy plugin" $SOURCE \
  org.apache.ivy.feature.feature.group \
  org.apache.ivy.eclipse.ant.feature.feature.group \
  org.apache.ivyde.feature.feature.group
